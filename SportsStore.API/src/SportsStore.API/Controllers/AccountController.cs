﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SportsStore.API.Models;
using SportsStore.API.Options;
using SportsStore.API.Services;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SportsStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly SigningCredentials _signingCredentials;
        private readonly JsonSerializerSettings _serializerSettings;
        private AppSettings ConfigSettings { get; set; }
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly IAccountService _userService;
        private readonly IGenericService<Role> _roleService;

        public AccountController(IOptions<AppSettings> settings, 
                    IOptions<JwtIssuerOptions> jwtOptions, 
                    IAccountService userService, 
                    IGenericService<Role> roleService)
        {
            _userService = userService;
            _roleService = roleService;
            _jwtOptions = jwtOptions.Value;

            ConfigSettings = settings.Value;
            string secretKey = ConfigSettings.SecretKey;
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            _signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Register([FromBody] ApplicationUser applicationUser)
        {
            object jsonResult = null;
            //set the user as a generic user
            var role = _roleService.GetById<Role>(m => m.Name == "GenericUser");
            applicationUser.Role = role.Result;

            if (ModelState.IsValid)
            {
                var result = _userService.Add(applicationUser, m => m.Email == applicationUser.Email);
                if (result == string.Empty)
                {
                    IEnumerable<Claim> claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, role.Result.Name, ClaimValueTypes.String, Issuer),
                        new Claim(JwtRegisteredClaimNames.Jti, JtiGenerator, ClaimValueTypes.String, Issuer),
                        new Claim(ClaimTypes.Role, "ValidUsers", Issuer),
                        new Claim(ClaimTypes.Name, role.Result.Name)
                    };
                    var claimsIdentity = new ClaimsIdentity(claims.ToList()[0].Value);
                    claimsIdentity.AddClaims(claims.ToList());
                    return TokenResponse(claimsIdentity);
                }
                jsonResult = new { Success = "False", Message = result };
            }
            return Json(jsonResult);
        }


        [HttpPost]
        [AllowAnonymous]
        public IActionResult Login([FromBody] ApplicationUser applicationUser)
        {
            var claims = GetValidUserClaims(applicationUser);
            if (claims == null)
                return Json(new { Success = "False", Message = "Incorrect Login Details"});

            var claimsIdentity = new ClaimsIdentity(claims.ToList()[0].Value);
            claimsIdentity.AddClaims(claims.ToList());
            return TokenResponse(claimsIdentity);
        }

        private IEnumerable<Claim> GetValidUserClaims(ApplicationUser user)
        {
            var validUser = ValidLogin(user);
            if (validUser == null) return null;

            //get the Role for this RoleId
            var roleName = _roleService.GetById<Role>(x => x.Id == validUser.RoleId).Result.Name;
            IEnumerable<Claim> claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, roleName, ClaimValueTypes.String, Issuer),
                new Claim(JwtRegisteredClaimNames.Jti, JtiGenerator, ClaimValueTypes.String, Issuer),
                new Claim(ClaimTypes.Role, "ValidUsers", Issuer),
                new Claim(ClaimTypes.Name, roleName)
            };

            return claims;
        }

        private ApplicationUser ValidLogin(ApplicationUser user)
        {
            //check valid user
            return _userService.CheckValidLogin(user).Result;
        }

        private IActionResult TokenResponse(ClaimsIdentity identity, SigningCredentials overrideSigningCredentials = null)
        {
            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: Issuer,
                audience: Issuer,
                claims: identity.Claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: overrideSigningCredentials ?? _signingCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            // Serialize and return the response
            var response = new
            {
                access_token = encodedJwt
            };

            var json = JsonConvert.SerializeObject(response, _serializerSettings);
            return new OkObjectResult(json);
        }

        private static string Issuer => "http://localhost:60000/";

        private static string JtiGenerator => Guid.NewGuid().ToString();
    }
}
