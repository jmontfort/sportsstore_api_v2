﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SportsStore.API.Models;
using SportsStore.API.Services;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace SportsStore.API.Controllers
{
    [Authorize(Policy = "OnlyValidUsers")]
    [Route("api/[controller]")]
    public class CategoriesController : Controller
    {
        private readonly IGenericService<Category> _categoriesService;

        public CategoriesController(IGenericService<Category> categoriesService)
        {
            _categoriesService = categoriesService;
        }

        [HttpGet("/api/Categories/Get", Name = "GetCategories")]
        public async Task<IActionResult> Get()
        {
            var categories = await _categoriesService.GetAll();
            return Json(categories);
        }

        [HttpGet("/api/Categories/Get/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var category = await _categoriesService.GetById<Category>(m => m.Id == id);
            return Json(category);
        }

        [HttpGet("/api/Categories/Get/name={name}")]
        public async Task<IActionResult> Get(string name)
        {
            var categories = await _categoriesService.GetAll();
            categories = categories.FindAll(x=> x.Name == name);
            return Json(categories);
        }

        [Authorize(Policy = "Administrator")]
        [HttpPost]
        public IActionResult Create([FromBody] Category category)
        {
            if (category == null)
                return BadRequest();

            var result = _categoriesService.Add(category, m => m.Name == category.Name);
            if (result == string.Empty)
            {
                return CreatedAtRoute("GetCategories", new { id = category.Id }, category);

            }
            return BadRequest("Item not added");
        }

        [Authorize(Policy = "Administrator")]
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] Category category)
        {
            if (category == null || category.Id != id)
            {
                return BadRequest();
            }

            _categoriesService.Update(category);
            return new NoContentResult();
        }

        [Authorize(Policy = "Administrator")]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var category = _categoriesService.GetById<Category>(m => m.Id == id);
            _categoriesService.Delete(category.Result);
            return new NoContentResult();
        }
    }
}
