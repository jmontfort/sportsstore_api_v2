﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SportsStore.API.Models;

namespace SportsStore.API.Data
{
    public class DbInitializer
    {
        public static void Initialize(SportsStoreCFContext context)
        {
            context.Database.EnsureCreated();

            // Look for any products.
            if (context.Products.Any())
            {
                return; // DB has been seeded
            }

            #region Countries

            context.Countries.AddRange(
                new Country
                {
                    Name = "Great Britain",
                    Type = "1 Tier",
                    Code = "GB"
                },
                new Country
                {
                    Name = "Germany",
                    Type = "1 Tier",
                    Code = "DE"
                },
                new Country
                {
                    Name = "France",
                    Type = "1 Tier",
                    Code = "FR"
                },
                new Country
                {
                    Name = "Malta",
                    Type = "2 Tier",
                    Code = "MT"
                }
            );

            context.SaveChanges();

            #endregion

            #region Roles

            context.Roles.AddRange(
                new Role
                {
                    Name = "GenericUser"
                },
                new Role
                {
                    Name = "Administrator"
                },
                new Role
                {
                    Name = "SuperUser"
                }
            );

            context.SaveChanges();

            #endregion

            #region AddressType
            context.AddressTypes.AddRange(
                new AddressType
                {
                    Name = "Shipping"
                },
                new AddressType
                {
                    Name = "Billing"
                }
            );
            context.SaveChanges();
            #endregion

            #region Users

            context.ApplicationUsers.AddRange(
                new ApplicationUser
                {
                    Name = "Test",
                    Email = "test@test.com",
                    Surname = "Account",
                    HomeNo = "",
                    MobNo = "",
                    UserName = "testaccount",
                    Password = "81uYvEggJMXpcYY0V+vSRfdFyRr0yp/lipegdsu5rmZTwvfb", //Pas$word,
                    Role = context.Roles.FirstOrDefault(x => x.Name == "Administrator")
                },
                new ApplicationUser
                {
                    Name = "Normal",
                    Email = "normal@test.com",
                    Surname = "User",
                    HomeNo = "",
                    MobNo = "",
                    UserName = "testaccount",
                    Password = "j2eRE9qndiRdjXYN9FaGj01/mHpPQYIHBUh6FbYc8oV/OCx9", //NoRmaL,
                    Role = context.Roles.FirstOrDefault(x => x.Name == "GenericUser")
                }
            );
            context.SaveChanges();
            #endregion

            #region Address
            context.Addresses.AddRange(
                new Address
                {
                    Country = context.Countries.FirstOrDefault(x=> x.Name == "Malta"),
                    Address1 = "Flat 1",
                    Address2 = "Triq il Bigilla",
                    Address3 = "",
                    City = "Hamrun",
                    PostCode = "HMR 015",
                    AddressType = context.AddressTypes.FirstOrDefault(x => x.Name == "Shipping"),
                    User = context.ApplicationUsers.FirstOrDefault(x => x.Name == "Test" && x.Surname == "Account")
                }    
            );
            context.SaveChanges();
            #endregion

            #region Images

            context.Images.AddRange(
                new Image
                {
                    Name = "Adidas Brand Image",
                    Url = "/Brands/adidas_logo.png"
                },
                new Image
                {
                    Name = "Nike Brand Image",
                    Url = "/Brands/nike_logo.png"
                },
                new Image
                {
                    Name = "Puma Brand Image",
                    Url = "/Brands/puma_logo.png"
                },
                new Image
                {
                    Name = "Adidas Women's Shoes",
                    Url = "/Products/adidas_shoes_women.png"
                },
                new Image
                {
                    Name = "Nike Men's Shoes",
                    Url = "/Products/nike_shoes_men.png"
                },
                new Image
                {
                    Name = "Puma Children's Shoes",
                    Url = "/Products/puma_shoes_children.png"
                }
            );
            context.SaveChanges();

            #endregion

            #region Brands

            context.Brands.AddRange(
                new Brand
                {
                    Name = "Adidas",
                    Image = context.Images.FirstOrDefault(x => x.Url == "/Brands/adidas_logo.png")
                },
                new Brand
                {
                    Name = "Nike",
                    Image = context.Images.FirstOrDefault(x => x.Url == "/Brands/nike_logo.png")
                },
                new Brand
                {
                    Name = "Puma",
                    Image = context.Images.FirstOrDefault(x => x.Url == "/Brands/puma_logo.png")
                }
            );
            context.SaveChanges();

            #endregion

            #region Categories
            context.Categories.AddRange(
                new Category
                {
                    Name = "Men"
                },
                new Category
                {
                    Name = "Women"
                },
                new Category
                {
                    Name = "Children"
                },
                new Category
                {
                    Name = "Unisex"
                }
            );
            context.SaveChanges();
            #endregion

            #region Products
            context.Products.AddRange(
                    new Product
                    {
                        Brand = context.Brands.FirstOrDefault(x => x.Name == "Nike"),
                        Category = context.Categories.FirstOrDefault(x=>x.Name=="Men"),
                        Deal = false,
                        Description = "this is a test product",
                        Discount = 0,
                        Name = "Nike Shoes",
                        Price = new decimal(50.00),
                        Stock = 5
                    },
                    new Product
                    {
                        Brand = context.Brands.FirstOrDefault(x => x.Name == "Adidas"),
                        Category = context.Categories.FirstOrDefault(x => x.Name == "Women"),
                        Deal = false,
                        Description = "this is a another test product",
                        Discount = 0,
                        Name = "Adidas Shoes",
                        Price = new decimal(65.00),
                        Stock = 5
                    },
                    new Product
                    {
                        Brand = context.Brands.FirstOrDefault(x => x.Name == "Puma"),
                        Category = context.Categories.FirstOrDefault(x => x.Name == "Children"),
                        Deal = false,
                        Description = "this is a another test product",
                        Discount = 0,
                        Name = "Puma Shoes",
                        Price = new decimal(65.00),
                        Stock = 5
                    }

                );
            context.SaveChanges();
            #endregion

            #region ProductImages

            context.ProductImages.AddRange(
                    new ProductImage
                    {
                        Image = context.Images.FirstOrDefault(x => x.Url == "/Products/nike_shoes_men.png"),
                        Product = context.Products.FirstOrDefault(x => x.Name == "Nike Shoes")
                    },
                    new ProductImage
                    {
                        Image = context.Images.FirstOrDefault(x => x.Url == "/Products/adidas_shoes_women.png"),
                        Product = context.Products.FirstOrDefault(x => x.Name == "Adidas Shoes")
                    },
                    new ProductImage
                    {
                        Image = context.Images.FirstOrDefault(x => x.Url == "/Products/puma_shoes_children.png"),
                        Product = context.Products.FirstOrDefault(x => x.Name == "Puma Shoes")
                    }
                );
            context.SaveChanges();
            #endregion

        }
    }
}
