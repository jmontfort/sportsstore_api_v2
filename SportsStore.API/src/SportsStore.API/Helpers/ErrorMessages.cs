﻿using System.ComponentModel;

namespace SportsStore.API.Helpers
{
    public enum ErrorMessages
    {
        [Description("Record Exists")]
        RecordExists,

        [Description("This Email Address Already Exists")]
        EmailExists,
    }
}
