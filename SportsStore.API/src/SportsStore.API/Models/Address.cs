﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.API.Models
{
    public class Address
    {
        public long Id { get; set; }
        //public short CountryId { get; set; }
        //public short AddressTypeId { get; set; }
        //public long UserId { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        [Required(ErrorMessage = "This field is required")]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Only characters are allowed")]
        public string City { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string PostCode { get; set; }

        public AddressType AddressType { get; set; }
        public Country Country { get; set; }
        public ApplicationUser User { get; set; }
    }
}
