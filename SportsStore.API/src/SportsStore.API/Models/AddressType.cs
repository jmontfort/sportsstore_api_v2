﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsStore.API.Models
{
    public class AddressType
    {
        public short Id { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }

    }
}