﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.API.Models
{
    public class ApplicationUser
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Surname { get; set; }
        public string HomeNo { get; set; }
        public string MobNo { get; set; }
        public short RoleId { get; set; }

        public string UserName { get; set; }
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        public string Password { get; set; }

        public Role Role { get; set; }

    }
}
