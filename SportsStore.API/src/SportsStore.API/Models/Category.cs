﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.API.Models
{
    public class Category
    {
        public short Id { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }
    }
}
