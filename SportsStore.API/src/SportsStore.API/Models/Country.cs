using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SportsStore.API.Models
{
    public class Country
    {
        public short Id { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Code { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Type { get; set; }
    }
}