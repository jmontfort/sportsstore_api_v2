﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SportsStore.API.Models
{
    public class ProductImage
    {

        public long ImageId { get; set; }
        public long ProductId { get; set; }

        public Image Image { get; set; }
        public Product Product { get; set; }
    }
}
