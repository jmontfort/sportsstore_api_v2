﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SportsStore.API.Models
{
    public partial class SportsStoreCFContext : DbContext
    {
        public SportsStoreCFContext(DbContextOptions<SportsStoreCFContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<AddressType> AddressTypes { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Image> Images { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductImage> ProductImages { get; set; }
        public virtual DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public virtual DbSet<Role> Roles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AddressType>().ToTable("AddressType");
            modelBuilder.Entity<Address>().ToTable("Address");
            modelBuilder.Entity<Brand>().ToTable("Brand");
            modelBuilder.Entity<Category>().ToTable("Category");
            modelBuilder.Entity<Country>().ToTable("Country");
            modelBuilder.Entity<Image>().ToTable("Image");
            modelBuilder.Entity<Product>().ToTable("Product");
            modelBuilder.Entity<Role>().ToTable("Role");

            modelBuilder.Entity<ProductImage>().HasKey(x => new { x.ImageId, x.ProductId });
            modelBuilder.Entity<ProductImage>().HasOne(p => p.Product).WithMany(pi => pi.ProductImages).HasForeignKey(p => p.ProductId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ProductImage>().HasOne(i => i.Image).WithMany(pi => pi.ProductImages).HasForeignKey(i => i.ImageId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ProductImage>().ToTable("ProductImage");

            modelBuilder.Entity<ApplicationUser>().HasOne(c => c.Role)
                                            .WithMany(r => r.Users)
                                            .HasForeignKey(c => c.RoleId);
            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser");
        }
    }
}
