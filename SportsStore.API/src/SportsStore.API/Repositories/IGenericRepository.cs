﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SportsStore.API.Repositories
{
    public interface IGenericRepository<T>
    {
        Task<T> Get<TKey>(Expression<Func<T, bool>> filter = null, string includeProperties = "");
        Task<List<T>> GetAll(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = null);
        string Add(T entity, Expression<Func<T, bool>> filter = null);
        void Update(T entity);
        void Delete(T entity);
    }
}
