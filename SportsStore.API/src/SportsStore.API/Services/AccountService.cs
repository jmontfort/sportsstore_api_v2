﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Threading.Tasks;
using SportsStore.API.Helpers;
using SportsStore.API.Models;
using SportsStore.API.Repositories;

namespace SportsStore.API.Services
{
    public class AccountService : GenericService<ApplicationUser>, IAccountService
    {
        public AccountService(IGenericRepository<ApplicationUser> genericRepository) : base(genericRepository)
        {
        }

        string IAccountService.Add(ApplicationUser user, Expression<Func<ApplicationUser, bool>> filter = null)
        {
            var cryptedPassword = CryptPassword(user.Password);
            user.Password = cryptedPassword;
            var message = Add(user, filter);

            if (message == ErrorCodes.Exists.ToString())
            {
                return EnumDescription.GetEnumDescription(ErrorMessages.EmailExists);
            }
            return message;
        }

        async Task<ApplicationUser> IAccountService.CheckValidLogin(ApplicationUser user)
        {
            //check that the user exists in the database
            var verifiedUser = await GetById<ApplicationUser>(m => m.Email == user.Email);
            if (verifiedUser == null) return null;
            //check password
            var enteredPassword = user.Password;
            var dbPassword = verifiedUser.Password;
            return CheckPassword(enteredPassword, dbPassword) ? verifiedUser : null;
        }

        public string CryptPassword(string userPassword)
        {
            //Create the salt value with a cryptographic PRNG
            byte[] salt;
            using (var rng = RandomNumberGenerator.Create())
            {
                //Use like RNGCryptoServiceProvider
                rng.GetBytes(salt = new byte[16]);
            }

            //Create the Rfc2898DeriveBytes and get the hash value
            var pbkdf2 = new Rfc2898DeriveBytes(userPassword, salt, 10000);
            var hash = pbkdf2.GetBytes(20);

            //Combine the salt and password bytes for later use
            var hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            //Turn the combined salt+hash into a string for storage
            return Convert.ToBase64String(hashBytes);
        }

        public bool CheckPassword(string enteredPassword, string dbPassword)
        {
            /* Extract the bytes */
            var hashBytes = Convert.FromBase64String(dbPassword);
            /* Get the salt */
            var salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            /* Compute the hash on the password the user entered */
            var pbkdf2 = new Rfc2898DeriveBytes(enteredPassword, salt, 10000);
            var hash = pbkdf2.GetBytes(20);
            /* Compare the results */
            for (var i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return false;
                else
                    return true;
            return false;
        }
    }

}
