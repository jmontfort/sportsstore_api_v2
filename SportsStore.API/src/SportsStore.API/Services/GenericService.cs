﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SportsStore.API.Repositories;

namespace SportsStore.API.Services
{
    public class GenericService<T> : IGenericService<T> where T : class
    {
        private readonly IGenericRepository<T> _genericRepository;

        public GenericService(IGenericRepository<T> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        public async Task<List<T>> GetAll(
            Func<IQueryable<T>,
                IOrderedQueryable<T>> orderBy = null,
            string includeProperties = null)
        {
            return await _genericRepository.GetAll(orderBy, includeProperties);
        }

        public async Task<T> GetById<TKey>(
            Expression<Func<T, bool>> filter = null,
            string includeProperties = "")
        {
            return await _genericRepository.Get<T>(filter, includeProperties);
        }

        public string Add(T entity, Expression<Func<T, bool>> filter = null)
        {
            var errorMessage = string.Empty;
            try
            {
                errorMessage = _genericRepository.Add(entity, filter);
                if (errorMessage == string.Empty)
                    return string.Empty;
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return errorMessage;
        }

        public bool Update(T entity)
        {
            _genericRepository.Update(entity);
            return true;
        }

        public void Delete(T entity)
        {
            _genericRepository.Delete(entity);
        }

    }
}
