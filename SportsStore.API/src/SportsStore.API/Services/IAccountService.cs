﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SportsStore.API.Models;

namespace SportsStore.API.Services
{
    public interface IAccountService
    {
        string Add(ApplicationUser user, Expression<Func<ApplicationUser, bool>> filter = null);
        Task<ApplicationUser> CheckValidLogin(ApplicationUser user);
    }

}