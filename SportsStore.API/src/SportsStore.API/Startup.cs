﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SportsStore.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Mvc.Authorization;
using SportsStore.API.Data;
using SportsStore.API.Options;
using SportsStore.API.Repositories;
using SportsStore.API.Services;

namespace SportsStore.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment("Development"))
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddOptions();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy("OnlyValidUsers",
                    builder => builder.WithOrigins("http://localhost:4200", "http://localhost:60000")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
            });

            //var connection = @" Server=.;Database=SportsStore2;Trusted_Connection=True;MultipleActiveResultSets=true";
            //services.AddDbContext<SportsStore2Context>(options => options.UseSqlServer(connection));

            // Add framework services.
            services.AddDbContext<SportsStoreCFContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("SportsStoreCFConnection")));
            //services.AddDbContext<SportsStoreCFContext>(options =>
            //    options.UseSqlServer(Configuration.GetSection("ConnectionStrings").Value));

            // Make authentication compulsory across the board (i.e. shut
            // down EVERYTHING unless explicitly opened up).

            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });

            // Set up authorisation policies.
            services.AddAuthorization(options =>
            {
                options.AddPolicy("OnlyValidUsers",
                    policy =>
                    {
                        policy.RequireRole("ValidUsers");
                    });

                options.AddPolicy("GenericUser",
                    policy =>
                    {
                        policy.RequireUserName("GenericUser");
                        //policy.RequireClaim(ClaimTypes.Country, "UnitedKingdom");
                    });
                options.AddPolicy("Administrator",
                    policy =>
                    {
                        policy.RequireUserName("Administrator");
                        //policy.RequireClaim(ClaimTypes.Country, "UnitedKingdom");
                    });
                options.AddPolicy("SuperUser",
                    policy =>
                    {
                        policy.RequireUserName("SuperUser");
                        //policy.RequireClaim(ClaimTypes.Country, "UnitedKingdom");
                    });

            });

            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddTransient(typeof(IGenericService<>), typeof(GenericService<>));
            services.AddTransient(typeof(IAccountService), typeof(AccountService));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, SportsStoreCFContext context)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            const string domain = "http://localhost:60000/";
            string secretKey = Configuration.GetSection("AppSettings").GetSection("SecretKey").Value;
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = domain,

                ValidateAudience = true,
                ValidAudience = domain,

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = securityKey
            };

            // The order in which middleware is configured and added is important
            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });

            app.UseCors("OnlyValidUsers");

            app.UseMvc();
                
            //initialize seed data
            DbInitializer.Initialize(context);
        }
    }
}