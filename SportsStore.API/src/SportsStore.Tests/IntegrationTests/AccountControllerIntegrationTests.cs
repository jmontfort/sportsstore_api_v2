﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.PlatformAbstractions;
using NUnit.Framework;
using NUnit.Framework.Internal;
using SportsStore.API;
using SportsStore.API.Models;

namespace SportsStore.Tests.IntegrationTests
{
    [TestFixture()]
    public class AccountControllerIntegrationTests
    {
        private HttpClient _client;
        private Category _testCategory;
        private string _request;
        private List<Category> _categories;
        private Category _category;
        private readonly Utils _utils = new Utils();

        [SetUp]
        public void Setup()
        {
            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            var projectPath = Path.GetFullPath(Path.Combine(basePath, "../../../../SportsStore.Tests"));

            var server = new TestServer(Utils.GetHostBuilder(new string[] { })
                .UseContentRoot(projectPath)
                .UseEnvironment("Development")
                .UseStartup<Startup>());


            _client = server.CreateClient();

            _request = Enums.GetEnumDescription(Enums.Requests.Account);
        }

        [Test]
        public async Task Post_EnsureSuccessResponse_AccountController()
        {
            //Arrange
            var userDetails = new { Email = "test@test.com", Password = "Pas$word" };
            //_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //Act
            var postResponse = await _client.PostAsJsonAsync(_request + "Login", userDetails);
            postResponse.EnsureSuccessStatusCode();

            //Assert
            Assert.IsTrue(true);
        }


    }
}
