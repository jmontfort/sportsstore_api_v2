﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using NUnit.Framework;
using SportsStore.API;
using SportsStore.API.Models;
using SportsStore.API.Options;

namespace SportsStore.Tests.IntegrationTests
{
    [TestFixture]
    public class CategoriesControllerIntegrationTests
    {
        private HttpClient _client;
        private Category _testCategory;
        private string _request;
        private List<Category> _categories;
        private Category _category;
        private readonly Utils _utils = new Utils();

        [SetUp]
        public void Setup()
        {
            var basePath = PlatformServices.Default.Application.ApplicationBasePath;
            var projectPath = Path.GetFullPath(Path.Combine(basePath, "../../../../SportsStore.Tests"));

            var server = new TestServer(Utils.GetHostBuilder(new string[] { })
                .UseContentRoot(projectPath)
                .UseEnvironment("Development")
                .UseStartup<Startup>());


            _client = server.CreateClient();

            _testCategory = new Category
            {
                Name = Enums.GetEnumDescription(Enums.CategoryTestData.Name)
            };
            _request = Enums.GetEnumDescription(Enums.Requests.Categories);
        }


        [Test]
        public async Task Get_ReturnsAListOfCategories_EnsureSuccessResponse_CategoriesController()
        {
            //Arrange
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("Administrator"));

            //Act
            var response = await _client.GetAsync(_request + "Get");
            response.EnsureSuccessStatusCode();

            //Assert
            Assert.IsTrue(true);
        }

        [Test]
        public void Get_ReturnsAListOfCategories_CountIsBiggerThanZero_CategoriesController()
        {
            //Arrange
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("Administrator"));

            //Act
            _categories = GetAllCategories().Result;
            
            //Assert
            Assert.IsTrue(_categories.Count > 0);
        }

        [Test]
        public void Get_OneCategory_AssertIdBiggerThanZero_CategoriesController()
        {
            //Arrange
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("Administrator"));

            //Act
            _category = GetAllCategories().Result.FirstOrDefault();

            //Assert
            Assert.IsTrue(_category.Id > 0);
        }

        [Test]
        public void Create_CreateACategory_IfAdministrator_CategoriesController()
        {
            //Arrange
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("Administrator"));

            //Act
            var createCategoryTuple = CreateCategory();

            // Assert
            Assert.IsTrue(createCategoryTuple.Item1.IsSuccessStatusCode);
            Assert.IsTrue(createCategoryTuple.Item2.IsSuccessStatusCode);

            Assert.AreEqual(_testCategory.Name, createCategoryTuple.Item3.Name);
            Assert.AreEqual(_testCategory.Name, createCategoryTuple.Item4.Name);

            Assert.AreNotEqual(Guid.Empty, createCategoryTuple.Item3.Id);
            Assert.AreEqual(_testCategory.Id, createCategoryTuple.Item4.Id);
        }


        [Test]
        public void Create_CannotCreateACategory_IfNormalUser_CategoriesController()
        {
            //Arrange
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("GenericUser"));

            //Act
            var categoryTuple = CreateCategory();

            // Assert
            Assert.IsTrue(categoryTuple.Item1.StatusCode == (HttpStatusCode)403);
            Assert.IsTrue(categoryTuple.Item2 == null);

            Assert.AreEqual(null, categoryTuple.Item3);
            Assert.AreEqual(null, categoryTuple.Item4);
        }

        [Test]
        public void Update_UpdateACategory_IfAdministrator_CategoriesController()
        {
            //Arrange 
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("Administrator"));

            //Act
            var updateCategoryTuple = UpdateCategory();

            // Assert
            Assert.IsTrue(updateCategoryTuple.Item1.IsSuccessStatusCode);
            Assert.IsTrue(updateCategoryTuple.Item3.IsSuccessStatusCode);
            Assert.IsTrue(updateCategoryTuple.Item3.StatusCode == HttpStatusCode.NoContent);
            Assert.IsTrue(updateCategoryTuple.Item2.IsSuccessStatusCode);

            Assert.AreEqual(Enums.GetEnumDescription(Enums.CategoryUpdatedTestData.Name), updateCategoryTuple.Item5.Name);

            Assert.AreNotEqual(Guid.Empty, updateCategoryTuple.Item4.Id);
            Assert.AreEqual(updateCategoryTuple.Item4.Id, updateCategoryTuple.Item5.Id);

        }


        [Test]
        public void Update_CannotUpdateACategory_IfGenericUser_CategoriesController()
        {
            //Arrange 
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("GenericUser"));

            //Act
            var updateCategoryTuple = UpdateCategory();

            // Assert

            Assert.IsTrue(updateCategoryTuple.Item1.StatusCode == (HttpStatusCode)403);
            Assert.IsTrue(updateCategoryTuple.Item2 == null);
            Assert.IsTrue(updateCategoryTuple.Item3 == null);

            Assert.AreEqual(null, updateCategoryTuple.Item4);
            Assert.AreEqual(null, updateCategoryTuple.Item5);
        }

        [Test]
        public void Delete_CannotDeleteACategory_IfGenericUser_CategoriesController()
        {
            //Arrange 
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("GenericUser"));

            //Act
            var deleteCategoryTuple = DeleteCategory();

            //Assert
            Assert.IsTrue(deleteCategoryTuple.Item1.StatusCode == (HttpStatusCode)403);
            Assert.IsTrue(deleteCategoryTuple.Item2 == null);
            Assert.IsTrue(deleteCategoryTuple.Item3 == null);

            Assert.AreEqual(null, deleteCategoryTuple.Item4);
            Assert.AreEqual(null, deleteCategoryTuple.Item5);
        }

        [Test]
        public void Delete_DeleteACategory_IfAdministrator_CategoriesController()
        {
            //Arrange 
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _utils.GetToken("Administrator"));

            //Act
            var deleteCategoryTuple = DeleteCategory();


            //Assert
            Assert.IsTrue(deleteCategoryTuple.Item1.IsSuccessStatusCode);
            Assert.IsTrue(deleteCategoryTuple.Item2.IsSuccessStatusCode);
            Assert.IsTrue(deleteCategoryTuple.Item2.StatusCode == HttpStatusCode.NoContent);
            Assert.IsTrue(deleteCategoryTuple.Item3.IsSuccessStatusCode);

            Assert.AreEqual(_testCategory.Name, deleteCategoryTuple.Item4.Name);
            Assert.AreNotEqual(Guid.Empty, deleteCategoryTuple.Item4.Id);

            if (deleteCategoryTuple.Item5.Count > 0)
                Assert.IsTrue(deleteCategoryTuple.Item5.Any(x => x.Id == deleteCategoryTuple.Item4.Id == false));

        }

        private async Task<List<Category>> GetAllCategories()
        {
            var getResponse = await _client.GetAsync(_request + "Get");
            var fetchedCategories = getResponse.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Category>>(fetchedCategories.Result);
        }

        private Tuple<HttpResponseMessage, HttpResponseMessage, Category, Category> CreateCategory()
        {
            return GetCreateCategoryTuple();
        }

        public Tuple<HttpResponseMessage, HttpResponseMessage, HttpResponseMessage, Category, Category> UpdateCategory()
        {
            return GetUpdateCategoryTuple().Result;

        }

        public Tuple<HttpResponseMessage, HttpResponseMessage, HttpResponseMessage, Category, List<Category>> DeleteCategory()
        {
            return GetDeleteCategoryTuple().Result;

        }

        private Tuple<HttpResponseMessage, HttpResponseMessage, Category, Category> GetCreateCategoryTuple()
        {
            var postCreatedCategoryTuple = GetPostCreatedCategoryTuple().Result;

            //if its not an Administrator, cannot create a Category
            if (postCreatedCategoryTuple.Item2 == null)
                return new Tuple<HttpResponseMessage, HttpResponseMessage, Category, Category>(
                    postCreatedCategoryTuple.Item1, null, null, null);

            _testCategory.Id = postCreatedCategoryTuple.Item2.Id;

            var createCreatedCategoryTuple = GetCreatedCategoryTuple(postCreatedCategoryTuple.Item2).Result;

            return new Tuple<HttpResponseMessage, HttpResponseMessage, Category, Category>(
                postCreatedCategoryTuple.Item1, createCreatedCategoryTuple.Item1,
                postCreatedCategoryTuple.Item2, createCreatedCategoryTuple.Item2);
        }

        private async Task<Tuple<HttpResponseMessage, HttpResponseMessage, HttpResponseMessage, Category, Category>> GetUpdateCategoryTuple()
        {
            var postCreatedCategoryTuple = GetPostCreatedCategoryTuple().Result;

            if (postCreatedCategoryTuple.Item2 == null)
                return new Tuple<HttpResponseMessage, HttpResponseMessage, HttpResponseMessage, Category, Category>(
                    postCreatedCategoryTuple.Item1, null, null, null, null);
                
            //PUT(Update)
            _testCategory.Id = postCreatedCategoryTuple.Item2.Id;
            _testCategory.Name = Enums.GetEnumDescription(Enums.CategoryUpdatedTestData.Name);
            var putResponse = await _client.PutAsJsonAsync(_request + postCreatedCategoryTuple.Item2.Id, _testCategory);

            //GET
            var createCreatedCategoryTuple = GetCreatedCategoryTuple(postCreatedCategoryTuple.Item2).Result;
            _testCategory.Id = createCreatedCategoryTuple.Item2.Id;

            return new Tuple<HttpResponseMessage, HttpResponseMessage, HttpResponseMessage, Category, Category>(
                    postCreatedCategoryTuple.Item1, createCreatedCategoryTuple.Item1, putResponse, 
                    postCreatedCategoryTuple.Item2, createCreatedCategoryTuple.Item2);
        }

        private async Task<Tuple<HttpResponseMessage, HttpResponseMessage, HttpResponseMessage, Category, List<Category>>> GetDeleteCategoryTuple()
        {
            var postCreatedCategoryTuple = GetPostCreatedCategoryTuple().Result;

            if (postCreatedCategoryTuple.Item2 == null)
                return new Tuple<HttpResponseMessage, HttpResponseMessage, HttpResponseMessage, Category, List<Category>>(
                    postCreatedCategoryTuple.Item1, null, null, null, null);

            //DELETE
            var deleteResponse = await _client.DeleteAsync(_request + postCreatedCategoryTuple.Item2.Id);
            var getResponse = await _client.GetAsync(_request + "Get");
            var all = getResponse.Content.ReadAsStringAsync();
            var allCategories = JsonConvert.DeserializeObject<List<Category>>(all.Result);

            return new Tuple<HttpResponseMessage, HttpResponseMessage, HttpResponseMessage, Category, List<Category>>(
                postCreatedCategoryTuple.Item1, deleteResponse, getResponse, postCreatedCategoryTuple.Item2, allCategories);
        }

        private async Task<Tuple<HttpResponseMessage, Category>> GetPostCreatedCategoryTuple()
        {
            var postResponse = await _client.PostAsJsonAsync(_request, _testCategory);
            var created = await postResponse.Content.ReadAsStringAsync();
            var createdCategory = JsonConvert.DeserializeObject<Category>(created);

            return new Tuple<HttpResponseMessage, Category>(
                postResponse, createdCategory);
        }

        private async Task<Tuple<HttpResponseMessage, Category>> GetCreatedCategoryTuple(Category createdCategory)
        {
            var getResponse = await _client.GetAsync(_request + "Get/" + createdCategory.Id);
            var fetched = await getResponse.Content.ReadAsStringAsync();
            var fetchedCategory = JsonConvert.DeserializeObject<Category>(fetched);

            return new Tuple<HttpResponseMessage, Category>(
                getResponse, fetchedCategory);
        }

        [TearDown]
        public async Task CleanUp()
        {
            //Cleanup
            if (_testCategory != null && _testCategory.Id > 0
                && (_testCategory.Name == Enums.GetEnumDescription(Enums.CategoryTestData.Name)
                || _testCategory.Name == Enums.GetEnumDescription(Enums.CategoryUpdatedTestData.Name)))
            {
                await _client.DeleteAsync(_request + _testCategory.Id);
                _testCategory = null;
            }
        }


    }
}
