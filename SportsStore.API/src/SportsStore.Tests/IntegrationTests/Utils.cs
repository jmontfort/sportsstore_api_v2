﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using SportsStore.API;
using SportsStore.API.Options;

namespace SportsStore.Tests.IntegrationTests
{
    public class Utils
    {
        private static string Issuer => "http://localhost:60000/";
        private static string JtiGenerator => Guid.NewGuid().ToString();

        private JwtIssuerOptions _jwtOptions;
        private SigningCredentials _signingCredentials;
        private AppSettings ConfigSettings { get; set; }
        private JsonSerializerSettings _serializerSettings;

        public static IWebHostBuilder GetHostBuilder(string[] args)
        {
            var config = new ConfigurationBuilder()
                       .AddCommandLine(args)
                       .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                       .Build();

            return new WebHostBuilder()
                .UseConfiguration(config)
                .UseKestrel()
                .UseStartup<Startup>();
        }

        public string GetToken(string claimName)
        {
            var builder = new ConfigurationBuilder();
            var config = builder.Build();
            var options = new AppSettings() { SecretKey = "thisismysecretkey" };
            config.GetSection("AppSettings").Bind(options);

            var secretKey = options.SecretKey;
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            _signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            _serializerSettings = new JsonSerializerSettings { Formatting = Formatting.Indented };
            _jwtOptions = new JwtIssuerOptions();

            var claimsIdentity = GetClaimsIdentity(claimName);
            return CreateToken(claimsIdentity);

        }

        private ClaimsIdentity GetClaimsIdentity(string claimName)
        {
            var claims = GetValidUserClaims(claimName);
            var claimsIdentity = new ClaimsIdentity(claims.ToList()[0].Value);
            claimsIdentity.AddClaims(claims.ToList());
            return claimsIdentity;
        }

        private IEnumerable<Claim> GetValidUserClaims(string claimName)
        {
            IEnumerable<Claim> claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, claimName, ClaimValueTypes.String, Issuer),
                new Claim(JwtRegisteredClaimNames.Jti, JtiGenerator, ClaimValueTypes.String, Issuer),
                new Claim(ClaimTypes.Role, "ValidUsers", Issuer),
                new Claim(ClaimTypes.Name, claimName)
            };

            return claims;
        }

        private string CreateToken(ClaimsIdentity identity, SigningCredentials overrideSigningCredentials = null)
        {
            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: Issuer,
                audience: Issuer,
                claims: identity.Claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: overrideSigningCredentials ?? _signingCredentials);

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
    }
}
